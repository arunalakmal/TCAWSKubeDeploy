data "template_file" "user-init-kube-master" {
  template = file("${path.module}/files/kube_master.tpl")
}

data "aws_iam_policy" "instance_policy" {
  arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}